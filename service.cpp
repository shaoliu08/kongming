#include <fstream>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <fastcgi++/request.hpp>
#include <fastcgi++/manager.hpp>
#include <string>


void error_log(const char* msg) {
	using namespace std;
   	using namespace boost;
   	static ofstream error;
   	if(!error.is_open()) {
    	error.open("/tmp/infer_errlog", ios_base::out | ios_base::app);
    	error.imbue(locale(error.getloc(), new posix_time::time_facet()));
   	}
   	error << '[' << posix_time::second_clock::local_time() << "] " << msg << endl;
}

class Infer: public Fastcgipp::Request<char>
{
public:
	Infer()
	{	
	}
	~Infer()
	{
	}
protected:
   	bool response(){
      		if(environment().gets.size()==1) {
                const std::string& d1 = environment().findGet("data"); 
                //const std::string& p1 = environment().findPost("data").value;//environment().findPost("data").value;
			    //out << environment().findPost("data").value;
			    //std::basic_string<char> data = environment().findPost("data").value;
			    //Fastcgipp::Http::Environment<wchar_t>::Posts::const_iterator it=environment().posts.begin();
			    //boost::shared_array<wchar_t> data = it->second.data;
			    //charT* data = it->second.data.get();
                char * content = new char[d1.length()+1];
                std::strcpy (content, d1.c_str());
      			out << content; //infer(content, "0", "0");
      		}
      		return true;
   	}
};

int main() {
    try
    {
        Fastcgipp::Manager<Infer> fcgi;
    	fcgi.handler();
    }
    catch(std::exception& e)
    {
        error_log(e.what());
    }
}
