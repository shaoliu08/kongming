#include <iostream>
#include "fun.h"
#include "crf.h"
#include "utf8.h"

std::string UnicodeToUTF8(unsigned int codepoint)
{
    std::string out;

    if (codepoint <= 0x7f)
        out.append(1, static_cast<char>(codepoint));
    else if (codepoint <= 0x7ff)
    {
        out.append(1, static_cast<char>(0xc0 | ((codepoint >> 6) & 0x1f)));
        out.append(1, static_cast<char>(0x80 | (codepoint & 0x3f)));
    }
    else if (codepoint <= 0xffff)
    {
        out.append(1, static_cast<char>(0xe0 | ((codepoint >> 12) & 0x0f)));
        out.append(1, static_cast<char>(0x80 | ((codepoint >> 6) & 0x3f)));
        out.append(1, static_cast<char>(0x80 | (codepoint & 0x3f)));
    }
    else
    {
        out.append(1, static_cast<char>(0xf0 | ((codepoint >> 18) & 0x07)));
        out.append(1, static_cast<char>(0x80 | ((codepoint >> 12) & 0x3f)));
        out.append(1, static_cast<char>(0x80 | ((codepoint >> 6) & 0x3f)));
        out.append(1, static_cast<char>(0x80 | (codepoint & 0x3f)));
    }
    return out;
}

CRF* init(char *model_file, char *margin, char *seqp, char *nbest) {
	//check necessary parameters
	if (!model_file[0]) {
		std::cout<<"no model file"<<std::endl;
		return NULL;
	}
	
	CRF *c=new CRF();
	c->set_para("margin",margin);
	c->set_para("seqp",seqp);
	c->set_para("nbest",nbest);
	c->load_model(model_file);	

	return c;
}

void infer(CRF *c, char *content, char *margin, char *seqp) {
    std::vector< std::vector<std::string> > table;

	int length = utf8::distance(content, content+strlen(content));
    utf8::iterator<char*> it(content, content, content+strlen(content));
    for(int i=0;i<length;i++) {
        std::string s = UnicodeToUTF8(*it);
		vector<string> row;
		row.push_back(s);
		table.push_back(row);
        it++;

    }

	int total=0;
	int error=0;
	int i,j,k;
	//convert to ext_table, val_table
	vector<vector<vector<string> > > ext_table(table.size());	//split each unit by " "
	vector<vector<vector<double> > > val_table(table.size());
	for (i=0;i<table.size();i++) {
		ext_table[i].resize(table[i].size());
		if (c->is_real) {
			val_table[i].resize(table[i].size());
		}
		for (j=0;j<table[i].size();j++) {
			char unit[100000];
			strcpy(unit,table[i][j].c_str());
			vector<char *> units;
			vector<double> uvals;//unit values
			split_string(unit," ",units);
			
			for (k=0;k<units.size();) {
				if (!units[k][0])
					units.erase(units.begin()+k);
				else {
					if (c->is_real) {
						if (j+1<table[i].size()) {
							char *q=strrchr(units[k],':');
							if (q==units[k]) {//	:0.5
								units.erase(units.begin()+k);
								continue;
							}
							*q=0;
							q++;
							uvals.push_back(atof(q));
						}else{
							uvals.push_back(0);
						}
					}
					k++;
				}
			}
			ext_table[i][j].resize(units.size());
			if(c->is_real) {
				val_table[i][j].resize(units.size());
				val_table[i][j]=uvals;
			}
			for(k=0;k<units.size();k++) {
				ext_table[i][j][k]=units[k];
			}
		}
	}
	
	vector < vector < string > > y;
	vector < double > sequencep;
	vector < vector < double > > nodep;
	c->tag(ext_table,val_table,y,sequencep,nodep);
	if(atoi(seqp)){
		for(i=0;i < sequencep.size()-1 ; i++)
			cout<<sequencep[i]<<"\t";
		cout<<sequencep[i]<<endl;
	}
	int table_rows=y[0].size();
	int table_cols=table[0].size();
	total+=table_rows;
	for(i=0;i < table_rows ; i++)
	{
		if(table[i].back()!=y[0][i])
			error++;
		for(j=0;j < table_cols;j++)
			cout<<table[i][j].c_str()<<"\t";
		for(j=0;j < y.size()-1;j++)
			cout<<y[j][i].c_str()<<"\t";
		cout<<y[j][i].c_str();
		if(atoi(margin)){
			cout<<"\t";
			for(j=0;j < nodep[0].size()-1; j++)
				cout<<nodep[i][j]<<"\t";
			cout<<nodep[i][j];
		}
		cout<<endl;
	}
	table.clear();
}

int main(int argc, char *argv[]) {

	if (argc == 2) {
		char *margin = "1";
		char *seqp = "1";
		char *nbest = "1";
		CRF *c = init(argv[1], margin, seqp, nbest);

		if (c) {
			std::string content;
			for (std::string line; std::getline(std::cin, line);) {
				if (line[0]==3) break;	//ctrl-c
				if (line[0]==7) {		//ctrl-g
    	    		content.append(1,0);
    	    		infer(c, &content[0], margin, seqp);
    	    		content.clear();
				} else {
					content += line;
				}
    		}
			delete c;
		}
	} else {
		std::cout<<"parameters error\n";
	}

	return 0;
}
