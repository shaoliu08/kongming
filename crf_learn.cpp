#include <fstream>
#include <iostream>
#include "fun.h"
#include "crf.h"
using namespace std;

//build learn
int main_learn(int argc, char *argv[]){
const char* learn_help="\
format:\n\
crf_learn template_file_name train_file_name model_file_name\n\
option type   default   meaning\n\
-c     double 1         Gaussian prior/L1 regularizer.\n\
-f     int    0         Frequency threshold.\n\
-p     int    1         Thread number. \n\
-i     int    10000     Max iteration number.\n\
-e     double 0.0001    Controls training precision.\n\
-a     int    0         Algorithm:\n\
                          0: CRFs\n\
                          1: Averaged Perceptron\n\
                          2: Passive Aggressive\n\
						  3: L1 CRFs\n\
-d     int    5         LBFGS depth.\n\
-m     int    0         0: fast train crf\n\
                        1: slow but requires less memory.\n\
";
//initial learning parameters
	char train_file[100]="";
	char templet_file[100]="";
	char model_file[100]="";
	char sigma[100]="1";		//-c
	char freq_thresh[100]="0";	//-f
	char thread_num[100]="1";	//-p
	char max_iter[100]="10000";	//-i
	char eta[100]="0.0001";	//-e
	char algorithm[100]="0";//-a
	char depth[100]="5";//-d
	char prior[100]="0";//-m
	//get learning parameters, and check
	int i=1;
	int step=0;//0: next load templet_file, 1: next load train_file, 2: next_load model_file
	while(i<argc){
		if(!strcmp(argv[i],"-h")){
			cout<<learn_help<<endl;
			return 0;
		}else if(!strcmp(argv[i],"-c")){
			if(i+1==argc){
				cout<<"-c parameter empty"<<endl;
				return 1;
			}
			strcpy(sigma,argv[i+1]);
			if(atof(sigma)<=0){
				cout<<"invalid -c parameter"<<endl;
				return 1;
			}
			i+=2;
		}else if(!strcmp(argv[i],"-f")){
			if(i+1==argc){
				cout<<"-f parameter empty"<<endl;
				return 1;
			}
			strcpy(freq_thresh,argv[i+1]);
			if(atoi(freq_thresh)<0){
				cout<<"invalid -f parameter"<<endl;
				return 1;
			}
			i+=2;
		}else if(!strcmp(argv[i],"-p")){
			if(i+1==argc){
				cout<<"-p parameter empty"<<endl;
				return 1;
			}
			strcpy(thread_num,argv[i+1]);
			if(atoi(thread_num)<0){
				cout<<"invalid -p parameter"<<endl;
				return 1;
			}
			i+=2;
		}else if(!strcmp(argv[i],"-m")){
			if(i+1==argc){
				cout<<"-m parameter empty"<<endl;
				return 1;
			}
			strcpy(prior,argv[i+1]);
			if(atoi(prior)<0 || atoi(prior)>1){
				cout<<"invalid -m parameter"<<endl;
				return 1;
			}
			i+=2;
		}else if(!strcmp(argv[i],"-i")){
			if(i+1==argc){
				cout<<"-i parameter empty"<<endl;
				return 1;
			}
			strcpy(max_iter,argv[i+1]);
			if(atoi(max_iter)<0){
				cout<<"invalid -i parameter"<<endl;
				return 1;
			}
			i+=2;
		}else if(!strcmp(argv[i],"-e")){
			if(i+1==argc){
				cout<<"-e parameter empty"<<endl;
				return 1;
			}
			strcpy(eta,argv[i+1]);
			if(atof(eta)<0){
				cout<<"invalid -e parameter"<<endl;
				return 1;
			}
			i+=2;
		}else if(!strcmp(argv[i],"-a")){
			if(i+1==argc){
				cout<<"-a parameter empty"<<endl;
				return 1;
			}
			strcpy(algorithm,argv[i+1]);
			if(atoi(algorithm)<0||atoi(algorithm)>3){
				cout<<"invalid -a parameter"<<endl;
				return 1;
			}
			i+=2;
		}else if(!strcmp(argv[i],"-d")){
			if(i+1==argc){
				cout<<"-d parameter empty"<<endl;
				return 1;
			}
			strcpy(depth,argv[i+1]);
			if(atoi(depth)<=0){
				cout<<"invalid -d parameter"<<endl;
				return 1;
			}
			i+=2;
		}else if(argv[i][0]=='-'){
			cout<<argv[i]<<": invalid parameter"<<endl;
			return 1;
		}else if(step==0){
			strcpy(templet_file,argv[i]);
			i++;
			step++;
		}else if(step==1){
			strcpy(train_file,argv[i]);
			i++;
			step++;
		}else if(step==2){
			strcpy(model_file,argv[i]);
			i++;
			step++;
		}
	}
	//check necessary parameters
	if(!templet_file[0]){
		cout<<"no template file"<<endl;
		return 1;
	}
	if(!train_file[0]){
		cout<<"no train file"<<endl;
		return 1;
	}
	if(!model_file[0]){
		cout<<"no model file"<<endl;
		return 1;
	}
	CRF *c=new CRF();
	c->set_para("sigma",sigma);
	c->set_para("freq_thresh",freq_thresh);
	c->set_para("max_iter",max_iter);
	c->set_para("thread_num",thread_num);
	c->set_para("eta",eta);
	c->set_para("algorithm",algorithm);
	c->set_para("depth",depth);
	c->set_para("prior",prior);
	c->learn(templet_file,train_file,model_file);
	delete c;
	return 0;
}

int main(int argc, char *argv[])
{
	return main_learn(argc,argv);
}
