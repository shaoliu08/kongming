CC=g++
CFLAGS=-w -Wall -O3 -Wno-deprecated
LIB1=-lpthread -lm -dl  -lstdc++ 
LIB2=-lfastcgipp -lboost_system -lboost_thread
all: crf_learn crf_test crf_infer 

clean:
	rm -f *.o
	rm -f crf_learn crf_test crf_infer

crf_thread.o: crf_thread.cpp crf_thread.h
		$(CC) $(CFLAGS) -c crf_thread.cpp -o crf_thread.o

crf.o: crf.cpp crf.h fun.h lbfgs.h thread.h crf_thread.h dat.h
		$(CC) $(CFLAGS) -c crf.cpp -o crf.o

lbfgs.o: lbfgs.cpp lbfgs.h
		$(CC) $(CFLAGS) -c lbfgs.cpp -o lbfgs.o

fun.o: fun.cpp fun.h
		$(CC) $(CFLAGS) -c fun.cpp -o fun.o

crf_learn.o: crf_learn.cpp crf.h fun.h
		$(CC) $(CFLAGS) -c crf_learn.cpp -o crf_learn.o

crf_test.o: crf_test.cpp crf.h fun.h
		$(CC) $(CFLAGS) -c crf_test.cpp -o crf_test.o

crf_infer.o: crf_infer.cpp crf.h fun.h
		$(CC) $(CFLAGS) -c crf_infer.cpp -o crf_infer.o

crf_learn: crf_learn.o crf.o crf_thread.o lbfgs.o fun.o
		$(CC) $(CFLAGS) crf_learn.o crf.o crf_thread.o lbfgs.o fun.o -o crf_learn $(LIB1)

crf_test: crf_test.o crf.o crf_thread.o lbfgs.o fun.o
		$(CC) $(CFLAGS) crf_test.o crf.o crf_thread.o lbfgs.o fun.o -o crf_test $(LIB1)

crf_infer: crf_infer.o crf.o crf_thread.o lbfgs.o fun.o
		$(CC) $(CFLAGS) crf_infer.o crf.o crf_thread.o lbfgs.o fun.o -o crf_infer $(LIB1)
