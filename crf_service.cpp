#include <fstream>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <fastcgi++/request.hpp>
#include <fastcgi++/manager.hpp>
#include <string>
#include <iostream>
#include "fun.h"
#include "crf.h"


void error_log(const char* msg) {
	using namespace std;
   	using namespace boost;
   	static ofstream error;
   	if(!error.is_open()) {
    	error.open("/tmp/infer_errlog", ios_base::out | ios_base::app);
    	error.imbue(locale(error.getloc(), new posix_time::time_facet()));
   	}
   	error << '[' << posix_time::second_clock::local_time() << "] " << msg << endl;
}

class Infer: public Fastcgipp::Request<char>
{
public:
	CRF *crf;
	Infer()
	{	
		char model[] = "crf_model";
		char margin[] = "0";
		char seqp[] = "0";
		char nbest[] = "1";
		init(model, margin, seqp, nbest);
	}
	~Infer()
	{
		if (crf) {
			delete crf;
		}
	}
 
	void init(char *model_file, char *margin, char *seqp, char *nbest) {
		//check necessary parameters
		if (!model_file[0]) {
			out<<"no model file";
		} else {		
			CRF *crf=new CRF();
			crf->set_para("margin",margin);
			crf->set_para("seqp",seqp);
			crf->set_para("nbest",nbest);
			crf->load_model(model_file);	
		}
	}

	string infer(char *content, char *margin, char *seqp) {
		std::vector< std::vector<std::string> > table;
        
        for(char* it = content; *it; ++it) 
        {
            vector<string> row;
            row.push_back(string(1,*it));
            table.push_back(row);
        }
        /*
		if (content[0]) {
			char *pc=content, *line;
			while (line=strstr(pc,"\n")) {
				*line=0;

				vector<string> row;
				char *pr=pc,*col;
				while (col=strstr(pr,"\t")) {
					*col=0;
					row.push_back(pr);
					pr=col+1;
				}
				row.push_back(pr);
				table.push_back(row);	

				pc=line+1;
			}
		}
        */

		int total=0;
		int error=0;
		int i,j,k;
		string rc;
		//convert to ext_table, val_table
		vector<vector<vector<string> > > ext_table(table.size());	//split each unit by " "
		vector<vector<vector<double> > > val_table(table.size());
		for (i=0;i<table.size();i++) {
			ext_table[i].resize(table[i].size());
			if (crf->is_real) {
				val_table[i].resize(table[i].size());
			}
			for (j=0;j<table[i].size();j++) {
				char unit[100000];
				strcpy(unit,table[i][j].c_str());
				vector<char *> units;
				vector<double> uvals;//unit values
				split_string(unit," ",units);
				
				for (k=0;k<units.size();) {
					if (!units[k][0])
						units.erase(units.begin()+k);
					else {
						if (crf->is_real) {
							if (j+1<table[i].size()) {
								char *q=strrchr(units[k],':');
								if (q==units[k]) {//	:0.5
									units.erase(units.begin()+k);
									continue;
								}
								*q=0;
								q++;
								uvals.push_back(atof(q));
							}else{
								uvals.push_back(0);
							}
						}
						k++;
					}
				}
				ext_table[i][j].resize(units.size());
				if(crf->is_real) {
					val_table[i][j].resize(units.size());
					val_table[i][j]=uvals;
				}
				for(k=0;k<units.size();k++) {
					ext_table[i][j][k]=units[k];
				}
			}
		}
		
		vector < vector < string > > y;
		vector < double > sequencep;
		vector < vector < double > > nodep;
		crf->tag(ext_table,val_table,y,sequencep,nodep);
		//if(atoi(seqp)){
		//	for(i=0;i < sequencep.size()-1 ; i++)
		//		rc += std::to_string(sequencep[i]) + "\t";
		//	rc += std::to_string(sequencep[i]) + "\n";
		//}
		int table_rows=y[0].size();
		int table_cols=table[0].size();
		total+=table_rows;
		for(i=0;i < table_rows ; i++)
		{
			if(table[i].back()!=y[0][i])
				error++;
			for(j=0;j < table_cols;j++)
				rc += table[i][j] + "\t";

			for(j=0;j < y.size()-1;j++)
				rc += y[j][i] + "\t";
			rc += y[j][i];
			
			//if(atoi(margin)){
			//	rc += "\t";
			//	for(j=0;j < nodep[0].size()-1; j++)
			//		rc += nodep[i][j] + "\t";
			//	rc += nodep[i][j];
			//}
			rc += "\n";
		}
		table.clear();
	}

protected:
   	bool response(){
      		if(environment().gets.size()==1) {
                const std::string& d1 = environment().findGet("data"); 
                //const std::string& p1 = environment().findPost("data").value;//environment().findPost("data").value;
			    //out << environment().findPost("data").value;
			    //std::basic_string<char> data = environment().findPost("data").value;
			    //Fastcgipp::Http::Environment<wchar_t>::Posts::const_iterator it=environment().posts.begin();
			    //boost::shared_array<wchar_t> data = it->second.data;
			    //charT* data = it->second.data.get();
                char * content = new char[d1.length()+1];
                std::strcpy (content, d1.c_str());
      			out << infer(content, "0", "0");
      		}
      		return true;
   	}
};

int main() {
   try
   {
    	Fastcgipp::Manager<Infer> fcgi;
    	fcgi.handler();
   }
   catch(std::exception& e)
   {
      error_log(e.what());
   }
}
